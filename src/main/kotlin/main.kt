import org.w3c.dom.asList
import kotlin.browser.document

fun main(args: Array<String>) {
    console.info("start")
    console.info(js("Kotlin.isType(document.getElementsByTagName(\"li\")[0], Object)"))
    document.getElementsByTagName("li").asList().toList().onEach { console.info(it) }.first()
    console.info("finish")
}